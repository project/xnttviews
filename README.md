External Entitites Views plugin
===============================

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------

External Entities Views plugin enables the use of external entity data types in
views.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/xnttviews

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/xnttviews

REQUIREMENTS
------------

This module requires the following modules:

 * Views
 * [External Entities](https://www.drupal.org/project/external_entities)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration.

TROUBLESHOOTING
---------------

 * If:

   - Are ?

   - Does ?

FAQ
---

Q: I ?

A: Yes.

MAINTAINERS
-----------

Current maintainers:
 * Valentin Guignon (guignonv) - https://www.drupal.org/u/guignonv

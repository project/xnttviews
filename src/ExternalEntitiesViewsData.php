<?php

namespace Drupal\xnttviews;

use Drupal\views\EntityViewsDataInterface;

/**
 * @todo: test this approach as a patch to external entities module instead of the hook.
 * https://api.drupal.org/api/drupal/core%21modules%21views%21views.api.php/function/hook_views_data/9.4.x
 * add "handlers.views_data" = "Drupal\xnttviews\ExternalEntitiesViewsData"
 * in ExternalEntity class annotation.
 */
class ExternalEntitiesViewsData implements EntityViewsDataInterface {
  public function getViewsData() {
    
  }
  
  public function getViewsTableForEntityType(EntityTypeInterface $entity_type) {
    
  }

}

<?php

namespace Drupal\xnttviews\Plugin\views\query;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\views\ResultRow;
use Drupal\views\ViewExecutable;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a Views query class for External Entities.
 *
 * To complete implementation, see ad example:
 * https://git.drupalcode.org/project/search_api/-/blob/8.x-1.x/src/Plugin/views/query/SearchApiQuery.php?ref_type=heads
 *
 * @ViewsQuery(
 *   id = "xnttviews",
 *   title = @Translation("External Entities Query"),
 *   help = @Translation("The query will be generated and run on external entities.")
 * )
 */
class XnttViewsQuery extends QueryPluginBase {

  /**
   * The External Entity type ID.
   *
   * @var string
   */
  protected $xntt_type_id;
  protected $query;

 /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);
    $views_data = \Drupal\views\Views::viewsData()->get($view->storage->get('base_table'));
    $this->xntt_type_id =
      $views_data['table']['base']['xntt']
      ?? preg_replace('/^xnttviews_/', '', $view->storage->get('base_table'))
    ;

    $xntt_loader = \Drupal::service('entity_type.manager')->getStorage($this->xntt_type_id);
    $this->query = $xntt_loader->getQuery()->accessCheck(TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function ensureTable($table, $relationship = NULL) {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function addField($table, $field, $alias = '', $params = array()) {
    $this->fields[$field] = [
      'field' => $field,
      'table' => '',
      'alias' => $field,
    ];
    return $field;
  }

  /**
   *
   */
  public function addWhere($group, $field, $value = NULL, $operator = NULL) {
//\Drupal::logger('debug')->debug('DEBUG addWhere: ' . print_r($field, TRUE)); //+debug
    // Ensure all variants of 0 are actually 0. Thus '', 0 and NULL are all
    // the default group.
    if (empty($group)) {
      $group = 0;
    }
    // Check for a group.
    if (!isset($this->where[$group])) {
      $this->setWhereGroup('AND', $group);
    }
    $this->where[$group]['conditions'][] = [
      'field' => ltrim($field, '.'),
      'value' => $value,
      'operator' => $operator,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function addOrderBy($table, $field = NULL, $order = 'ASC', $alias = '', $params = []) {
    $this->orderby[] = [
      'field' => $field ?? '',
      'direction' => strtoupper($order),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function build(ViewExecutable $view) {
    $this->view = $view;
    // Initialize the pager and let it modify the query to add limits. This has
    // to be done even for aborted queries since it might otherwise lead to a
    // fatal error when Views tries to access $view->pager.
    $view->initPager();
    $view->pager->query();
  }

  /**
   * {@inheritdoc}
   */
  public function execute(ViewExecutable $view) {

    // Manage conditions.
    // @todo: manage complex conditions.
    if (isset($this->where)) {
      foreach ($this->where as $where_group => $where) {
// \Drupal::logger('debug')->debug('DEBUG where: ' . print_r($where, TRUE)); //+debug
        foreach ($where['conditions'] as $condition) {
          $this->query->condition(
            $condition['field'],
            $condition['value'],
            $condition['operator']
          );
        }
      }
    }

    $xntt_loader = \Drupal::service('entity_type.manager')->getStorage($this->xntt_type_id);
    $view->pager->preExecute($this->query);
    // Views passes sometimes NULL and sometimes the integer 0 for "All" in a
    // pager. If set to 0 items, a string "0" is passed. Therefore, we unset
    // the limit if an empty value OTHER than a string "0" was passed.
    if (!$this->limit && $this->limit !== '0') {
      $this->limit = NULL;
    }
    if (isset($this->offset)) {
      $this->query->range($this->offset, $this->limit);
    }

    foreach ($this->orderby ?? [] as $order) {
      $this->query->sort($order['field'], $order['direction']);
    }
    
    $start = microtime(TRUE);
    $count_query = clone $this->query;
    $xntts = $this->query->execute();
    $entities = $xntt_loader->loadMultiple(array_values($xntts));
    $view->execute_time = microtime(TRUE) - $start;

    $index = 0;
    foreach ($entities as $entity) {
      $row['index'] = $index++;
      $row['_entity'] = $entity;
      $fields = array_keys($this->fields ?? ['title' => 'title']);
      foreach ($fields as $field) {
        // $data = $entity->toArray();
        // $data[$field]
        // $row[$field] = $entity->get($field)->getString();
        // $row[$field] = [
        //    '#type' => 'markup',
        //    '#markup' => $entity->get($field)->getString(),
        // ];
        $row[$field] = $entity->get($field);
      }
      $view->result[] = new ResultRow($row);
    }

    $view->pager->total_items = $count_query->count()->execute();

    if (!empty($view->pager->options['offset'])) {
      $view->pager->total_items -= $view->pager->options['offset'];
    }
    $view->total_rows = $view->pager->total_items;

    // Fill info.
    // @todo: provide preview infos.
    // See /core/modules/views_ui/src/ViewUI.php (line 628...)
    $view->build_info = [
      // 'title' => 'External Entity Query',
      'query' => $this->query,
      'count_query' => $count_query,
      'query_args' => [],
    ]
    + $view->build_info
    ;

  }

}

<?php

namespace Drupal\xnttviews\Plugin\views\filter;

use Drupal\Core\Database\Connection;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\filter\StringFilter;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Xnttviews string filter.
 *
 * Including default string filters plus fuzzy search (when available).
 *
 * @ViewsFilter("xnttviewsstring")
 */
class XnttStringFilter extends StringFilter {

  /**
   * {@inheritdoc}
   */
  public function operators() {
    $operators = parent::operators();

    // Add fuzzy search.
    $operators += [
      'fuzzy' => [
        'title' => $this->t('Fuzzy search'),
        'short' => $this->t('fuzzy'),
        'method' => 'opFuzzy',
        'values' => 1,
      ],
    ];

    return $operators;
  }

  protected function opFuzzy($field) {
    $operator = $this->getConditionOperator('FUZZY');
    $this->query->addWhere($this->options['group'], $field, $this->value, $operator);
  }

}
